﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;

public class Gestionnaire : MonoBehaviour {
    public List<Texture> loadedTextures = new List<Texture>();
    public string[] urls;
    public int maxThread = 4;

    public int usedThread = 0;
    private string saveDir = "";
    private Texture lastDownload;

    // Use this for initialization
    IEnumerator Start () {
        saveDir = Application.persistentDataPath + "/textures/"; // Set the Path at Start

        if (!Directory.Exists(saveDir)) // if the folder doesn't exist, create it.
            Directory.CreateDirectory(saveDir);
        int i = 0;

        while(i < urls.Length)
        {
            string filePath = Path.Combine(saveDir, Path.GetFileName(urls[i]));

            if (File.Exists(filePath)) // Choose if it's local or net image            
                StartCoroutine(Get(filePath));            
            else
                yield return StartCoroutine(Download(urls[i]));            
            i++;
        }
	}

    IEnumerator Download(string url)
    {
        WWW request = new WWW(url);
        yield return request;
        lastDownload = request.texture;

        loadedTextures.Add(request.texture); // Add the texture to the list, it will be loaded in VRAM
        
        byte[] t = request.texture.EncodeToPNG();
        while (usedThread >= maxThread) yield return new WaitForEndOfFrame(); // Wait for free thread
        (new Thread(() => Save(t, Path.GetFileName(url)))).Start();        
    }
	
    IEnumerator Get(string url)
    { 
        WWW request = new WWW( Application.isMobilePlatform ? url.Replace("C:" , "file://") : url );
        yield return request;

        loadedTextures.Add(request.texture); // Add the texture to the list, it will be loaded in VRAM
    }

    void Save(byte[] tex, string name)
    {
        usedThread++;
        File.WriteAllBytes(saveDir + name, tex); // And write it to the saving path

#if UNITY_EDITOR
        File.AppendAllText(saveDir + "log.txt", name + "\r\n");// Debug
#endif
        usedThread--;
        Thread.CurrentThread.Join(); // "Close" the thread
    }

    void OnGUI()
    {
        GUILayout.Label(saveDir);
    }
}
